package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
)

//SOAL 2
type NilaiMahasiswa struct{
	Nama, MataKuliah, IndeksNilai string
	Nilai, ID uint
  }
  
  var nilaiNilaiMahasiswa = []NilaiMahasiswa{
	{"Andi", "Kalkulus", "B", 75, 1101156},
	{"Siska", "Kimia", "A", 80, 1101146},
	{"Alif", "Marketing", "A", 85, 1101198},
  }

  func getIndeksNilai(nilai uint) string{
	switch{
	case nilai >= 80: return "A"
	case nilai >= 70 && nilai <= 80: return "B"
	case nilai >= 60 && nilai <= 70: return "B"
	case nilai >= 50 && nilai <= 60: return "B"
	default: return "E"
	}
  }

  func getNilaiMahasiswa(w http.ResponseWriter, r *http.Request){
	if r.Method=="GET"{
		dataMahasiswa, err := json.Marshal(nilaiNilaiMahasiswa)

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(dataMahasiswa)
		return
	}
	http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
	return
  }

  //SOAL 1
  func PostNilaiMahasiswa(w http.ResponseWriter, r *http.Request){
	w.Header().Set("Content-Type", "application/json")
	var nilaiMhs = NilaiMahasiswa{
		ID : uint(len(nilaiNilaiMahasiswa)+1),
	}
	if r.Method == "POST"{
		if r.Header.Get("Content-Type") == "application/json"{
			decodeJson := json.NewDecoder(r.Body)
			if err := decodeJson.Decode(&nilaiMhs); err != nil{
				log.Fatal(err)
			}
		} else{
			//yang boleh diinput saat post
			nama := r.FormValue("nama")
			mataKuliah := r.FormValue("mata_kuliah")
			getNilai := r.FormValue("nilai")
			nilai, _ := strconv.Atoi(getNilai) 

			nilaiMhs.Nama = nama
			nilaiMhs.MataKuliah = mataKuliah
			nilaiMhs.Nilai = uint(nilai)
		}
		if nilaiMhs.Nilai > 100 {
			http.Error(w, "Nilai tidak boleh diinput lebih dari 100", http.StatusBadRequest)
			return
		}
		nilaiMhs.IndeksNilai = getIndeksNilai(nilaiMhs.Nilai)

		dataNilai,_ := json.Marshal(nilaiMhs)
		nilaiNilaiMahasiswa = append(nilaiNilaiMahasiswa, nilaiMhs)
		w.Write(dataNilai)
		return
	}
	http.Error(w, "Method Not Allowed", http.StatusMethodNotAllowed)
	return
  }

func Auth(next http.Handler)http.Handler{
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request){
		username, password, ok := r.BasicAuth()
		//cek username dan password kosong
		if !ok {
			w.Write([]byte("Username dan Password tidak boleh kosong"))
			return
		}

		//cek username dan password valid
		if username == "admin" && password == "admin"{
			next.ServeHTTP(w,r)
			return
		}
		w.Write([]byte("username atau password salah"))
		return
	})
}
  
func main() {
	//SOAL 2
	fmt.Println("________SOAL 2_______")
	http.HandleFunc("/mahasiswa", getNilaiMahasiswa)
	http.Handle("/mahasiswa/post", Auth(http.HandlerFunc(PostNilaiMahasiswa)))
	log.Fatal(http.ListenAndServe(":8888", nil))

}
