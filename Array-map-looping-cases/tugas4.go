package main

import "fmt"

func main() {
	//SOAL 1
	fmt.Println("_______SOAL 1________")
	for i:=1; i<=20; i++{
		if i%3==0 && i%2==1{
			fmt.Println(i, "-", "I Love Coding")
		} else if i%2==1{
			fmt.Println(i, "-", "Santai")
		} else if i%2==0{
			fmt.Println(i, "-", "Berkualitas")
		}
	}

	//SOAL 2
	fmt.Println("_______SOAL 2________")
	for i:=0; i<7; i++{
		for j:=0; j<=i; j++{
			fmt.Printf("#")
		}
		fmt.Printf("\n")
	}

	//SOAL 3
	fmt.Println("_______SOAL 3________")
	var kalimat = [...]string{"aku", "dan", "saya", "sangat", "senang", "belajar", "golang"}
	fmt.Println(kalimat[2:7]) //[saya sangat senang belajar golang]

	//SOAL 4
	fmt.Println("_______SOAL 4________")
	var sayuran = []string{}
	sayuran = append(sayuran, "Bayam", "Buncis", "Kangkung", "Kubis", "Seledri", "Tauge", "Timun" )
	
	for index, value := range sayuran {
		fmt.Println(index+1,".", value)
	}

	//SOAL 5
	fmt.Println("_______SOAL 5________")
	var satuan = map[string]int{
		"panjang": 7,
		"lebar":   4,
		"tinggi":  6,
	  }
	  panjang := satuan["panjang"]
	  lebar := satuan["lebar"]
	  tinggi := satuan["tinggi"]
	  volume := panjang * lebar * tinggi
	  fmt.Println("Panjang=",panjang,",", "Lebar=", lebar,",", "Tinggi=", tinggi,",", "Volume Balok=",volume)
}