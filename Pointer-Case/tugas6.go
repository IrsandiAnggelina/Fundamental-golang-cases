package main

import "fmt"

func main() {
	//SOAL 1
	fmt.Println("_________SOAL 1________")
	
	var luasLigkaran float64 
	var kelilingLingkaran float64

	hitungLingkaran(7, &luasLigkaran, &kelilingLingkaran)
	fmt.Println(luasLigkaran, kelilingLingkaran)

	hitungLingkaran(9, &luasLigkaran, &kelilingLingkaran)
	fmt.Println(luasLigkaran, kelilingLingkaran)

	//SOAL 2
	fmt.Println("_________SOAL 2________")
	var sentence string 
	introduce(&sentence, "John", "laki-laki", "penulis", "30")
	fmt.Println(sentence) // Menampilkan "Pak John adalah seorang penulis yang berusia 30 tahun"
	
	introduce(&sentence, "Sarah", "perempuan", "model", "28")
	fmt.Println(sentence) // Menampilkan "Bu Sarah adalah seorang model yang berusia 28 tahun"

	//SOAL 3
	fmt.Println("_________SOAL 3________")
	var buah = []string{}
	tambahBuah(&buah, "Jeruk", "Semangka", "Mangga", "Strawberry","Durian","Manggis","Alpukat")

	for i,val := range buah{
		fmt.Println(i+1, val)
	}

	//SOAL 4
	var dataFilm = []map[string]string{}

	tambahDataFilm("LOTR", "2 jam", "action", "1999", &dataFilm)
	tambahDataFilm("avenger", "2 jam", "action", "2019", &dataFilm)
	tambahDataFilm("spiderman", "2 jam", "action", "2004", &dataFilm)
	tambahDataFilm("juon", "2 jam", "horror", "2004", &dataFilm)

	for index, item := range dataFilm{
		fmt.Print(index+1)
		fmt.Print(". ")
		i:= 1
		for key, value := range item{
			if i == 1{
				fmt.Println(key, ":", value)
			}else{
				fmt.Println(" ", key, ":",value)
			}
			i++
			{
				fmt.Println()
			}
		}
	}
}

//Function SOAL 1
func hitungLingkaran(jariJari int, luas *float64, keliling *float64){
	if (jariJari%7 == 0){
		*luas = float64(22/7 * jariJari * jariJari)
		*keliling = float64(2 * 22/7 * jariJari)
	} else {
		*luas = float64(3.14 * float64(jariJari) * float64(jariJari))
		*keliling = float64(2 * 3.14 * float64(jariJari))
	}
}

//Function SOAL 2
func introduce(sentence *string, nama, gender, job, age string){
	prefix := "Pak"
	if gender == "Perempuan"{
		gender = "Bu"
	}
	*sentence = prefix +" "+ nama+" " + "adalah seorang"+" "+job+" "+"yang berusia"+" "+age+" "+"tahun"
}

//Function SOAL 3
func tambahBuah(buah *[]string, listBuah ...string){
	*buah = append(*buah, listBuah...)
}

//Function SOAL 4
func tambahDataFilm(judul, duration, genre, tahun string, dataFilm *[]map[string]string){
	film := map[string]string{"title": judul, "duration":duration, "genre":genre, "year":tahun}
	*dataFilm = append(*dataFilm, film)
}
