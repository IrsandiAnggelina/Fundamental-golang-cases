package main

import "fmt"

//SOAL 1
type buah struct {
	nama string
	warna string
	adaBijinya bool
	harga int
}

//SOAL 2
type segitiga struct{
	alas, tinggi int
}
type persegi struct{
	sisi int
}
type persegiPanjang struct{
	panjang, lebar int
}
func (s segitiga) luas() int {
	luas := (s.alas * s.tinggi)/2
	return luas
}
func (p persegi) luas() int{
	luas := (p.sisi * p.sisi)
	return luas
}
func (pp persegiPanjang) luas() int{
	luas := (pp.panjang * pp.lebar)
	return luas
}

// //SOAL 3
type phone struct{
	name, brand string
	year int
	colors []string
}
func (ph *phone) addColors(listWarna ...string){
	for _, color := range listWarna{
		ph.colors = append(ph.colors, color)
	}
}

// //SOAL 4
type movie struct{
	title, genre string
	duration, year int
}

func tambahDataFilm(judul string, durasi int, genre string, tahun int, dataFilm *[]movie){
	var films = movie{judul, genre, durasi, tahun}
	*dataFilm = append(*dataFilm, films)
}


func main(){
//SOAL 1
fmt.Println("______SOAL 1______")
var daftarBuah = []buah{
	{nama:"Nanas", warna: "Kuning",adaBijinya: false, harga: 9000},
	{nama:"Jeruk", warna: "Orange", adaBijinya:true,harga: 8000 },
	{nama:"Semangka", warna: "Hijau & merah", adaBijinya: true, harga: 10000 },
	{nama:"Pisang", warna: "Kuning", adaBijinya: false, harga: 5000 },
}
for _,items := range daftarBuah{
	fmt.Println(items)
}


// //SOAL 2
fmt.Println("________SOAL 2________")
var hitungLuasSegitiga = segitiga{alas: 3, tinggi: 4}
fmt.Println("Luas Segitiga:", hitungLuasSegitiga.luas())
var luasPersegi = persegi{sisi : 3}
fmt.Println("Luas Persegi:", luasPersegi.luas())
var luasPersegiPanjang = persegiPanjang{panjang: 3, lebar: 4}
fmt.Println("Luas Persegi Pajang:", luasPersegiPanjang.luas())

// //SOAL 3
fmt.Println("________SOAL 3________")
var phoneData = phone{name: "Samsung", brand: "Android", year: 2022}
fmt.Println(phoneData)
phoneData.addColors("Hitam", "Kuning", "Biru","Silver")
fmt.Println(phoneData)

// //SOAL 4
fmt.Println("_________SOAL 4_________")
var dataFilm = []movie{}

tambahDataFilm("LOTR", 120, "action", 1999, &dataFilm)
tambahDataFilm("avenger", 120, "action", 2019, &dataFilm)
tambahDataFilm("spiderman", 120, "action", 2004, &dataFilm)
tambahDataFilm("juon", 120, "horror", 2004, &dataFilm)

fmt.Println(dataFilm)
for index, item := range dataFilm{
	fmt.Println(index+1, ".")
	fmt.Println(" Title:", item.title)
	fmt.Println(" duration:", item.duration/60, "jam")
	fmt.Println(" genre:", item.genre)
	fmt.Println(" year:", item.year)
}
}