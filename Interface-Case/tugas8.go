package main

import (
	"fmt"
	"strconv"
)

//SOAL 1
type segitigaSamaSisi struct {
	alas, tinggi int
}

type persegiPanjang struct {
	panjang, lebar int
}

type tabung struct {
	jariJari, tinggi float64
}

type balok struct {
	panjang, lebar, tinggi int
}

type hitungBangunDatar interface {
	luas() int
	keliling() int
}

type hitungBangunRuang interface {
	volume() float64
	luasPermukaan() float64
}

func (ss segitigaSamaSisi) luas() int {
	luas := (ss.alas * ss.tinggi) / 2
	return luas
}
func (ss segitigaSamaSisi) keliling() int {
	keliling := (3*ss.alas)
	return keliling
}
func (pp persegiPanjang) luas() int {
	keliling := (pp.panjang * pp.lebar)
	return keliling
}
func (pp persegiPanjang) keliling() int {
	keliling := 2 * (pp.panjang + pp.lebar)
	return keliling
}
func (t tabung) volume() float64 {
	if int(t.jariJari)%7==0{
		return float64(22/7*t.jariJari*t.jariJari*t.tinggi)
	} else{
		return float64(3.14*t.jariJari*t.jariJari*t.tinggi)
	}
}
func (t tabung) luasPermukaan() float64 {
	if int(t.jariJari)%7==0{
		return (2*22/7*t.jariJari*(t.jariJari+t.tinggi))
	} else{
		return (2*3.14*t.jariJari*(t.jariJari+t.tinggi))
	}
}
func (bl balok) volume() float64 {
	volume := bl.panjang*bl.lebar*bl.tinggi
	return float64(volume)
}
func (bl balok) luasPermukaan() float64 {
	p:= float64(bl.panjang)
	l := float64(bl.lebar)
	t := float64(bl.tinggi)
	luasP := (2*p*l)+(2*p*t)+(2*l*t)
	return float64(luasP)
}

//SOAL 2
type phone struct{
	name, brand string
	year int
	colors []string
 }
 type deskripsiData interface{
	tampilData() string
 }
 func (ph phone) tampilData() string{
	var data = "Name: " +ph.name+ "\n" +
				"Brand: "+ph.brand+ "\n" +
				"Year: "+ strconv.Itoa(ph.year)+ "\n" +
				"Colors: "
				for i,val := range ph.colors{
					if i ==0 {
						data += val
					}else{
						data += ", "+ val
					}
				}
				return data
 }


//  //SOAL 3
 func luasPersegi(sisi uint, status bool) interface{}{
// 	if (sisi == 0) && status == true{
// 		return "Maaf anda belum menginput sisi dari persegi"
// 	} else if (sisi == 0) && status==false {
// 		return "nil"
// 	} else if status==false{
// 		return sisi
// 	} else if status==true {
// 		return "Luas persegi dengan sisi "+ strconv.Itoa(int(sisi))+" cm adalah "+ strconv.Itoa(int(sisi*sisi))+ " cm"
//  }

switch {
case sisi >0 && status:
	return "luas persegi dengan sisi " +strconv.Itoa(int(sisi))+ " cm adalah "+ strconv.Itoa(int(sisi*sisi))+ " cm"
case sisi >0 && !status:
	return sisi*sisi
case sisi == 0 && status:
	return "Maaf anda belum menginput sisi dari persegi"
default:
	return nil	

}
 }

func main() {
	//SOAL 1
	fmt.Println("_______SOAL 1_______")
	fmt.Println("_______SOAL 1: Bangun Datar_______")
	var segitiga1 hitungBangunDatar = segitigaSamaSisi{alas:7, tinggi:8} 
	var persegiPanjang1 hitungBangunDatar = persegiPanjang{panjang:7, lebar:8} 
	fmt.Println("Luas Segitiga Sama Sisi:",segitiga1.luas()) //28
	fmt.Println("Keliling Segitiga Sama Sisi:",segitiga1.keliling()) //21
	fmt.Println("Luas Persegi Panjang:", persegiPanjang1.luas()) //56
	fmt.Println("Keliling Persegi Panjang:", persegiPanjang1.keliling()) //30

	fmt.Println("_______SOAL 1: Bangun Ruang_______")
	var tabung1 hitungBangunRuang = tabung{jariJari: 7, tinggi: 10} 
	var balok1 hitungBangunRuang = balok{panjang:7, lebar:5, tinggi: 6} 
	fmt.Println("Volume Tabung:",tabung1.volume()) //1470 ..
	fmt.Println("Luas Permukaan Tabung:", tabung1.luasPermukaan()) //714 ..
	fmt.Println("Volume Balok:",balok1.volume()) //210 
	fmt.Println("Luas Permukaan Balok:", balok1.luasPermukaan()) //72..

	//SOAL 2
	fmt.Println("_______SOAL 2_______")
	var structPhone deskripsiData = phone{
		name:"Samsung Galaxy", 
		brand: "Samsung", 
		year:2020,
		colors: []string{"Mystic Bronze", "Mystic White", "Mystic Black"},
	}
	fmt.Println(structPhone.tampilData())

	// //SOAL 3
	fmt.Println("_______SOAL 3_______")
	fmt.Println(luasPersegi(4, true))
	fmt.Println(luasPersegi(8, false))
	fmt.Println(luasPersegi(0, true))
	fmt.Println(luasPersegi(0, false))

	//SOAL 4
	fmt.Println("_______SOAL 4_______")
	var prefix interface{}= "hasil penjumlahan dari "
	var kumpulanangkapertama interface{}=[]int{6,8}
	var kumpulanangkakedua interface{} =[]int{12,14}

	var jumlah int
	kalimat := prefix.(string)
	for _,val := range kumpulanangkapertama.([]int){
		kalimat += strconv.Itoa(val) + "+"
		jumlah += val
	}

	for index,item := range kumpulanangkakedua.([]int){
		jumlah += item
		if index == len(kumpulanangkakedua.([]int))-1{
			kalimat += strconv.Itoa(item)+ "=" + strconv.Itoa(jumlah)
		} else {
			kalimat += strconv.Itoa(item)+ "+"
		}
	}
	fmt.Println(kalimat)
}