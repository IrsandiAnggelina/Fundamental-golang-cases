package main

import (
	"fmt"
	"sort"
	"strconv"
	"sync"
	"time"
)

//SOAL 1
func urutData(wg *sync.WaitGroup, phones ...string){
	for i, val := range(phones){
		fmt.Print(i+1)
		fmt.Println(".", val)
		time.Sleep(time.Second)
	}
	wg.Done()
}

//SOAL 2
func getMovies(ch chan string, movies ...string){
	fmt.Println("List Movies: ")
	for i, val := range movies{
		number := i + 1
		ch <- strconv.Itoa(number) + ". " + val
	}
	close(ch)
}

//SOAL 3
func luasLingkaran(jariJari int, ch chan float64){
	if jariJari % 7 == 0{
		ch <- 22/7 * float64(jariJari) * float64(jariJari)
	} else{
		ch <- 3.14 * float64(jariJari) * float64(jariJari)
	}
}

func kelilingLingkaran(jariJari int, ch chan float64){
	if jariJari % 7 == 0{
		ch <- 2 * 22/7 * float64(jariJari)
	} else {
		ch <- 2 * 3.14 * float64(jariJari)
	}
}
func volumeTabung(jariJari int, t int, ch chan float64){
	if jariJari % 7 == 0 {
		ch <- 22/7 * float64(jariJari) *float64(jariJari) * float64(t)
	} else {
		ch <- 3.14  * float64(jariJari) *float64(jariJari) * float64(t)
	}
}

//SOAL 4
// func luasPersegiPanjang(p int, l int, ch chan int){
// 	ch <- p * l
// }
// func kelilingPersegiPanjang(p int, l int, ch chan int){
// 	ch <- 2 * (p+l)
// }
// func volumeBalok(p int, l int, t int, ch chan int){
// 	ch <- p * l * t
// }


func main() {
	//SOAL 1
	fmt.Println("_______SOAL 1______")
	var wg sync.WaitGroup
	var phones = []string{"Xiaomi", "Asus", "Iphone", "Samsung", "Oppo", "Realme", "Vivo"}
	sort.Strings(phones)

	wg.Add(1)
	go urutData(&wg, phones...)
	wg.Wait()

	//SOAL 2
	fmt.Println("_______SOAL 2______")
	var movies = []string{"Harry Potter", "LOTR", "SpiderMan", "Logan", "Avengers", "Insidious", "Toy Story"}
	moviesChannel := make(chan string)
	go getMovies(moviesChannel, movies...)
	for value := range moviesChannel {
	fmt.Println(value)
	}

	// //SOAL 3
	fmt.Println("_______SOAL 3______")
	ch1 := make(chan float64)
	go luasLingkaran(8, ch1)

	ch2 := make(chan float64)
	go kelilingLingkaran(14, ch2)

	ch3 := make(chan float64)
	go volumeTabung(20, 10, ch3)

	for i:=0; i<3; i++{
		select{
		case hasil1 := <- ch1:
			fmt.Println("Luas lingkaran:", hasil1)
		case hasil2 := <- ch2:
			fmt.Println("Keliling Lingkaran:", hasil2)
		case hasil3 := <- ch3:
			fmt.Println("Volume Tabung:", hasil3)
		}
	}


	// //SOAL 4
	// fmt.Println("_______SOAL 4______")
	// // buatlah perhitungan luas persegi panjang dan keliling persegi panjang dan volume balok, 
	// // gunakan select untuk menentukan hasil dari luas, keliling dan volume balok masing-masing
	// chan1 := make(chan int)
	// go luasPersegiPanjang(3, 4, chan1)

	// chan2 := make(chan int)
	// go kelilingPersegiPanjang(3, 4, chan2)

	// chan3 := make(chan int)
	// go volumeBalok(4, 5, 6, chan3)

	// for i:=0; i<3; i++{
	// 	select{
	// 	case result1 := <- ch1:
	// 		fmt.Println("Luas Persegi Panjang:", result1)
	// 	case result2 := <- ch2:
	// 		fmt.Println("Keliling Persegi Panjang:", result2)
	// 	case result3 := <- ch3:
	// 		fmt.Println("Volume Balok:", result3)
	// 	}
	// }


}