package main

import (
	"fmt"
	"strconv"
)

//SOAL 1
func KalimatDefer(kalimat string, tahun int){
	fmt.Println(kalimat, tahun)
}

//SOAL 2
func kelilingSegitigaSamaSisi(sisi int, status bool)(string, error){
	if sisi > 0 && status{
		return "keliling segitiga sama sisinya dengan sisi ", sisi, " cm adalah ", sisi * sisi, " cm"
	}  else if sisi > 0 && !status{
		return strconv.Itoa(sisi)
	} else if sisi == 0 && status{
		return 
		if err ==nil{
			fmt.Println(val)
		} else{
			fmt.Println(err.error())
		}
	} else {

	}
}

func main() {
	//SOAL 1
	fmt.Println("________SOAL 1________")
	defer KalimatDefer("Golang Backend Development", 2021)

	//SOAL 2
	fmt.Println("________SOAL 2________")
	fmt.Println(kelilingSegitigaSamaSisi(4, true))
	fmt.Println(kelilingSegitigaSamaSisi(8, false))
	fmt.Println(kelilingSegitigaSamaSisi(0, true))
	fmt.Println(kelilingSegitigaSamaSisi(0, false))
}