package controllers

import (
	"net/http"

	"final-project/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type pelangganInput struct {
	ID          uint      `gorm:"primary_key" json:"id"`
	Name        string    `json:"name"`
	Phone		uint    `json:"phone"`
	Email		string	  `json:"email"`
}

// GetAllPelanggan godoc
// @Summary Get all Pelanggan.
// @Description Get a list of Pelanggan.
// @Tags Pelanggan
// @Produce json
// @Success 200 {object} []models.Pelanggan
// @Router /Pelanggan [get]
func GetAllPelanggan(c *gin.Context) {
    // get db from gin context
    db := c.MustGet("db").(*gorm.DB)
    var pelanggan []models.Pelanggan
    db.Find(&pelanggan)

    c.JSON(http.StatusOK, gin.H{"data": pelanggan})
}

// CreatePelanggan godoc
// @Summary Create New Pelanggan.
// @Description Creating a new Pelanggan.
// @Tags Pelanggan
// @Param Body body pelangganInput true "the body to create a new Pelanggan"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Pelanggan
// @Router /pelanggan [post]
func CreatePelanggan(c *gin.Context) {
    // Validate input
    var input pelangganInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }
    pelanggan := models.Pelanggan{
		ID: input.ID, Name: input.Name, 
		Phone: input.Phone, Email: input.Email,
	}
    db := c.MustGet("db").(*gorm.DB)
    db.Create(&pelanggan)

    c.JSON(http.StatusOK, gin.H{"data": pelanggan})
}