package controllers

import (
	"net/http"
	"time"

	"final-project/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type pesananInput struct {
	IdKategori  	    uint    	     `json:"id_kategori"`
	NamaBarang  		string    	     `json:"nama_barang"`
	Jumlah  			uint    	     `json:"jumlah"`
	Harga  				uint   		     `json:"harga"`
	TotalBayar			uint		     `json:"total_bayar"`
}

// GetAllPesanan godoc
// @Summary Get all Pesanan.
// @Description Get a list of Pesanan.
// @Tags Pesanan
// @Produce json
// @Success 200 {object} []models.Pesanan
// @Router /pesanan [get]
func GetAllPesanan(c *gin.Context) {
    // get db from gin context
    db := c.MustGet("db").(*gorm.DB)
    var pesanan []models.Pesanan
    db.Find(&pesanan)

    c.JSON(http.StatusOK, gin.H{"data": pesanan})
}

// GetPesananById godoc
// @Summary Get Pesanan.
// @Description Get an Pesanan by id.
// @Tags Pesanan
// @Produce json
// @Param id path string true "Pesanan id"
// @Success 200 {object} models.Pesanan
// @Router /pesanan/{id} [get]
func GetPesananById(c *gin.Context) { 
    var pesanan models.Pesanan

    db := c.MustGet("db").(*gorm.DB)
    if err := db.Where("id = ?", c.Param("id")).First(&pesanan).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": pesanan})
}

// UpdatePesanan godoc
// @Summary Update Pesanan.
// @Description Update Pesanan by id.
// @Tags Pesanan
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "Pesanan id"
// @Param Body body pesananInput true "the body to update pesanan"
// @Success 200 {object} models.Pesanan
// @Router /pesanan/{id} [patch]
func UpdatePesanan(c *gin.Context) {

    db := c.MustGet("db").(*gorm.DB)
    var pesanan models.Pesanan
    if err := db.Where("id = ?", c.Param("id")).First(&pesanan).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }
    // Validate input
    var input pesananInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }
    var updatedInput models.Pesanan
    updatedInput.NamaBarang = input.NamaBarang
	updatedInput.Jumlah = input.Jumlah
    updatedInput.UpdatedAt = time.Now()

    db.Model(&pesanan).Updates(updatedInput)

    c.JSON(http.StatusOK, gin.H{"data": pesanan})
}

// DeletePesanan godoc
// @Summary Delete one Pesanan.
// @Description Delete a Pesanan by id.
// @Tags Pesanan
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "Pesanan id"
// @Success 200 {object} map[string]boolean
// @Router /pesanan/{id} [delete]
func DeletePesanan(c *gin.Context) {
    db := c.MustGet("db").(*gorm.DB)
    var pesanan models.Pesanan
    if err := db.Where("id = ?", c.Param("id")).First(&pesanan).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    db.Delete(&pesanan)

    c.JSON(http.StatusOK, gin.H{"data": true})
}