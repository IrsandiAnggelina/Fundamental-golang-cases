package models

import "time"

type Mahasiswa struct{
	ID uint `json:"id"`
	Nama string `json:"nama"`
	Skor uint `json:"skor"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}