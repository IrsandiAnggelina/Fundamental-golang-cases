package main

import (
	"fmt"
	"log"
	"net/http"
	"tugas-15/functions"

	"github.com/julienschmidt/httprouter"
)
  
func main() {
	router := httprouter.New()
	router.GET("/nilai-mahasiswa", functions.GetAllNilai)
	router.POST("/nilai-mahasiswa/create", functions.PostNilai)
	router.PUT("/nilai-mahasiswa/:id/delete", functions.UpdateNilai)
	router.DELETE("/nilai-mahasiswa/:id/delete", functions.DeleteNilai)

	router.GET("/mahasiswa", functions.GetAllMahasiswa)
	router.POST("/mahasiswa/create", functions.PostMahasiswa)
	router.PUT("/mahasiswa/:id/delete", functions.UpdateMahasiswa)
	router.DELETE("/mahasiswa/:id/delete", functions.DeleteMahasiswa)

	router.GET("/mata_kuliah", functions.GetAllMataKuliah)
	router.POST("/mata_kuliah/create", functions.PostMataKuliah)
	router.PUT("/mata_kuliah/:id/delete", functions.UpdateMataKuliah)
	router.DELETE("/mata_kuliah/:id/delete", functions.DeleteMataKuliah)

	fmt.Println("Server Running at Port 9090")
	log.Fatal(http.ListenAndServe(":9090", nil))

}
