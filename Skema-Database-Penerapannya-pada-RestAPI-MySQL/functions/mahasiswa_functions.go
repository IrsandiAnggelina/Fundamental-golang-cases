package functions

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"tugas-15/models"
	"tugas-15/query"
	"tugas-15/utils"

	"github.com/julienschmidt/httprouter"
)

func GetAllMahasiswa(w http.ResponseWriter, r *http.Request, _ httprouter.Params){
	ctx, cancel := context.WithCancel(context.Background())

	defer cancel()

	nilai, err := query.GetAllMahasiswa(ctx)
	if err != nil{
		fmt.Println(err)
	}

	utils.ResponseJSON(w, nilai, http.StatusOK)
}

func PostMahasiswa(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
    if r.Header.Get("Content-Type") != "application/json" {
        http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
        return
    }

    ctx, cancel := context.WithCancel(context.Background())
    defer cancel()

    var mhs models.Mahasiswa
    if err := json.NewDecoder(r.Body).Decode(&mhs); err != nil {
        utils.ResponseJSON(w, err, http.StatusBadRequest)
        return
    }

    if err := query.InsertMahasiswa(ctx, mhs); err != nil {
        utils.ResponseJSON(w, err, http.StatusInternalServerError)
        return
    } //insert Mahasiswa, yang di insert adalah mhs

    res := map[string]string{
        "status": "Succesfully",
    }

    utils.ResponseJSON(w, res, http.StatusCreated)
}

func UpdateMahasiswa(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
    if r.Header.Get("Content-Type") != "application/json" {
        http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
        return
    }

    ctx, cancel := context.WithCancel(context.Background())
    defer cancel()

    var mhs models.Mahasiswa
    if err := json.NewDecoder(r.Body).Decode(&mhs); err != nil {
        utils.ResponseJSON(w, err, http.StatusBadRequest)
        return
    }

	var idMhs = ps.ByName("id")

    if err := query.UpdateMahasiswa(ctx, mhs, idMhs); err != nil {
        utils.ResponseJSON(w, err, http.StatusInternalServerError)
        return
    }

    res := map[string]string{
        "status": "Succesfully",
    }
	
    utils.ResponseJSON(w, res, http.StatusCreated)
}

func DeleteMahasiswa(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
    ctx, cancel := context.WithCancel(context.Background())
    defer cancel()

   var idMhs = ps.ByName("id")

    if err := query.DeleteMahasiswa(ctx, idMhs); err != nil {
        utils.ResponseJSON(w, err, http.StatusInternalServerError)
        return
    }

    res := map[string]string{
        "status": "Succesfully",
    }
	
    utils.ResponseJSON(w, res, http.StatusCreated)
}