package functions

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"tugas-15/models"
	"tugas-15/query"
	"tugas-15/utils"

	"github.com/julienschmidt/httprouter"
)

func GetAllMataKuliah(w http.ResponseWriter, r *http.Request, _ httprouter.Params){
	ctx, cancel := context.WithCancel(context.Background())

	defer cancel()

	nilai, err := query.GetAllMataKuliah(ctx)
	if err != nil{
		fmt.Println(err)
	}

	utils.ResponseJSON(w, nilai, http.StatusOK)
}

func PostMataKuliah(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
    if r.Header.Get("Content-Type") != "application/json" {
        http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
        return
    }

    ctx, cancel := context.WithCancel(context.Background())
    defer cancel()

    var matkul models.Matakuliah
    if err := json.NewDecoder(r.Body).Decode(&matkul); err != nil {
        utils.ResponseJSON(w, err, http.StatusBadRequest)
        return
    }

    if err := query.InsertMataKuliah(ctx, matkul); err != nil {
        utils.ResponseJSON(w, err, http.StatusInternalServerError)
        return
    } //insert Mahasiswa, yang di insert adalah matkul

    res := map[string]string{
        "status": "Succesfully",
    }

    utils.ResponseJSON(w, res, http.StatusCreated)
}

func UpdateMataKuliah(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
    if r.Header.Get("Content-Type") != "application/json" {
        http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
        return
    }

    ctx, cancel := context.WithCancel(context.Background())
    defer cancel()

    var matkul models.Matakuliah
    if err := json.NewDecoder(r.Body).Decode(&matkul); err != nil {
        utils.ResponseJSON(w, err, http.StatusBadRequest)
        return
    }

	var idMatkul = ps.ByName("id")

    if err := query.UpdateMataKuliah(ctx, matkul, idMatkul); err != nil {
        utils.ResponseJSON(w, err, http.StatusInternalServerError)
        return
    }

    res := map[string]string{
        "status": "Succesfully",
    }
	
    utils.ResponseJSON(w, res, http.StatusCreated)
}

func DeleteMataKuliah(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
    ctx, cancel := context.WithCancel(context.Background())
    defer cancel()

   var idMatkul = ps.ByName("id")

    if err := query.DeleteMataKuliah(ctx, idMatkul); err != nil {
        utils.ResponseJSON(w, err, http.StatusInternalServerError)
        return
    }

    res := map[string]string{
        "status": "Succesfully",
    }
	
    utils.ResponseJSON(w, res, http.StatusCreated)
}