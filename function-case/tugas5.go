package main

import "fmt"

func main() {
//SOAL 1
fmt.Println("_________SOAL 1________")
luas := luasPersegiPanjang(12, 4)
keliling := kelilingPersegiPanjang(12, 4)
volume := volumeBalok(12, 4, 8)
fmt.Println("Luas:", luas) 
fmt.Println("Keliling:", keliling)
fmt.Println("Volume:", volume)

//SOAL 2
fmt.Println("_________SOAL 2________")
john := introduce("John", "laki-laki", "penulis", "30")
fmt.Println(john)
sarah := introduce2("Sarah", "perempuan", "model", "28")
fmt.Println(sarah)

//SOAL 3
fmt.Println("_________SOAL 3________")
buah := []string{"semangka", "jeruk", "melon", "pepaya"}
buahFavorit("Jhon", buah...)

//SOAL 4
fmt.Println("_________SOAL 4________")
// buatlah closure function dengan nama tambahDataFilm untuk
// menambahkan data map[string]string ke slice dataFilm
var dataFilm = []map[string]string{}

var tambahDataFilm = func(n1, n2, n3, n4 string) {
	var map1 = map[string]string{"genre": n3, "jam": n2, "tahun": n4, "title": n1}
	dataFilm = append(dataFilm, map1)
}

tambahDataFilm("LOTR", "2 jam", "action", "1999")
tambahDataFilm("avenger", "2 jam", "action", "2019")
tambahDataFilm("spiderman", "2 jam", "action", "2004")
tambahDataFilm("juon", "2 jam", "horror", "2004")

for _, item := range dataFilm {
	fmt.Println(item)
}
}


//SOAL 1
func luasPersegiPanjang(panjang,lebar int) int{
	luas := panjang * lebar
	return luas
}
func kelilingPersegiPanjang(panjang,lebar int) int {
	kll := 2 * (panjang * lebar)
	return kll
}
func volumeBalok(panjang,lebar,tinggi int) int {
	vol := panjang * lebar * tinggi
	return vol
}

//SOAL 2
func introduce(word1, word2, word3, word4 string) (data string){
	if word2 == "laki-laki"{
		word2 = "Pak"
	}
	data = word2+" "+word1+" "+"adalah seorang"+" "+word3+" "+"yang berusia"+" "+word4+" "+"tahun"
	return data
}

func introduce2(word1, word2, word3, word4 string) (data2 string){
	if word2 == "perempuan"{
		word2 = "Bu"
	}
	data2 = word2+" "+word1+" "+"adalah seorang"+" "+word3+" "+"yang berusia"+" "+word4+" "+"tahun"
	return data2
}

//SOAL 3
func buahFavorit(nama string, buah ...string) {
	fmt.Println("halo nama saya", nama, "dan buah favorit saya adalah : ")
	for _, buahfav := range buah {
		fmt.Println(buahfav)
	}
}

//SOAL 4 (ada di function main)
