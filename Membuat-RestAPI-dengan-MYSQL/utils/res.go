// folder utils untuk mencetak data dengan format JSON.
package utils

import (
	"encoding/json"
	"net/http"
)

func ResponseJSON(rw http.ResponseWriter, p interface{}, status int){
	ubahkeByte, err := json.Marshal(p)
	rw.Header().Set("Content-Type", "application/JSON")

	if err != nil{
		http.Error(rw, "Error", http.StatusBadRequest)
	}

	rw.Header().Set("Content-Type", "application/JSON")
	rw.WriteHeader(status)
	rw.Write([]byte(ubahkeByte))
}