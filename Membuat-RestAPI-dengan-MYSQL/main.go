package main

import (
	"fmt"
	"log"
	"net/http"
	"tugas-14-latihan/functions"

	"github.com/julienschmidt/httprouter"
)
  
func main() {
	router := httprouter.New()
	router.GET("/nilai-mahasiswa", functions.GetNilai)
	router.POST("/nilai-mahasiswa/create", functions.PostNilai)
	router.PUT("/nilai-mahasiswa/:id/delete", functions.UpdateNilai)
	router.DELETE("/nilai-mahasiswa/:id/delete", functions.DeleteNilai)

	fmt.Println("Server Running at Port 9090")
	log.Fatal(http.ListenAndServe(":9090", nil))

}
