//untuk melakukan konfigurasi MySQL.

// Sekarang kita lakukan koneksi ke database MySQL,
// untuk menghubungkan Golang dengan MySQL kita akan menggunakan package https://github.com/go-sql-driver/mysql
//yang sudah di install sebelumnya.

package config

import (
	"database/sql"
	"fmt"
)

const (
	username string = "root"
	password int = 12345
	database string = "db_tugas14"
)

//variable global
var (
	dsn = fmt.Sprintf("%v:%v@/%v", username, password, database)
)

//Hub to Mysql
func MYSQL() (*sql.DB, error){
	db, err := sql.Open("mysql", dsn)
	if err != nil{
		return nil, err
	}
	return db, err
}