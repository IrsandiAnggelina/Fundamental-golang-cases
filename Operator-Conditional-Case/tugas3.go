package main

import (
	"fmt"
	"strconv"
)

func main() {
//Soal 1
	fmt.Println("_________SOAL 1_________")
	var panjangPersegiPanjang string = "8"
	var lebarPersegiPanjang string = "5"
	var alasSegitiga string = "6"
	var tinggiSegitiga string = "7"

	intpanjangPersegiPanjang, _ := strconv.Atoi(panjangPersegiPanjang)
	intlebarPersegiPanjang,_ := strconv.Atoi(lebarPersegiPanjang)
	intalasSegitiga,_ := strconv.Atoi(alasSegitiga)
	inttinggiSegitiga, _:= strconv.Atoi(tinggiSegitiga)

	luasPersegiPanjang := intpanjangPersegiPanjang * intlebarPersegiPanjang
	kelilingPersegiPanjang := 2*(intpanjangPersegiPanjang+intlebarPersegiPanjang)
	luasSegitiga := (intalasSegitiga * inttinggiSegitiga)/2
	fmt.Println("Luas Persegi Panjang :",luasPersegiPanjang, ",", "Keliling Persegi Panjang:", kelilingPersegiPanjang,",","Luas Segitiga:", luasSegitiga)

//Soal 2
	fmt.Println("_________SOAL 2_________")
	var nilaiJohn = 80
	var nilaiDoe = 50

	switch{
	case nilaiJohn >= 80:
		fmt.Println("Indeksnya: A")
	case nilaiJohn >= 70 && nilaiJohn < 80:
		fmt.Println("Indeksnya: B")
	case nilaiJohn >= 60 && nilaiJohn < 70:
		fmt.Println("Indeksnya: C")
	case nilaiJohn >= 50 && nilaiJohn < 60:
		fmt.Println("Indeksnya: D")	
	default:
		fmt.Println("Indeksnya: E")
	}

	switch{
	case nilaiDoe >= 80:
		fmt.Println("Indeksnya: A")
	case nilaiDoe >= 70 && nilaiDoe < 80:
		fmt.Println("Indeksnya: B")
	case nilaiDoe >= 60 && nilaiDoe < 70:
		fmt.Println("Indeksnya: C")
	case nilaiDoe >= 50 && nilaiDoe < 60:
		fmt.Println("Indeksnya: D")	
	default:
		fmt.Println("Indeksnya: E")
	}

//Soal 3
	fmt.Println("_________SOAL 3_________")
	var tanggal = 17;
	var bulan = 8; 
	var tahun = 1945;

	tanggal = 06
	bulan = 9
	tahun = 1995
	
	switch bulan{
	case 1: fmt.Println(tanggal, "January", tahun)
	case 2: fmt.Println(tanggal, "February", tahun)
	case 3: fmt.Println(tanggal, "Maret", tahun)
	case 4: fmt.Println(tanggal, "April", tahun)
	case 5: fmt.Println(tanggal, "Mei", tahun)
	case 6: fmt.Println(tanggal, "Juni", tahun)
	case 7: fmt.Println(tanggal, "July", tahun)
	case 8: fmt.Println(tanggal, "Agustus", tahun)
	case 9: fmt.Println(tanggal, "September", tahun)
	case 10: fmt.Println(tanggal, "Oktober", tahun)
	case 11: fmt.Println(tanggal, "November", tahun)
	case 12: fmt.Println(tanggal, "Desember", tahun)
	default:
		fmt.Println("Unavailable")
	}

//Soal 4
	fmt.Println("_________SOAL 4_________")

	tahunLahir := 1995
	if tahunLahir >= 1944 && tahunLahir <= 1964{
		fmt.Println("Baby Boomer")
	} else if tahunLahir >= 1965 && tahunLahir <= 1979 {
		fmt.Println("Generasi X")
	} else if tahunLahir >= 1980 && tahunLahir <= 1994 {
		fmt.Println("Generasi Y")
	} else if tahunLahir >= 1995 && tahunLahir <= 2015 {
		fmt.Println("Generasi Z")
	}
}